FROM python:3.6
RUN pip3 install grpcio

ADD . /app/

WORKDIR /app/

RUN pip3 install pipenv

RUN pipenv lock -r >> requirements.txt

RUN pip3 install -r requirements.txt

EXPOSE 22222
