from concurrent import futures
import hashlib
import time
import hash_service_pb2
import hash_service_pb2_grpc
import grpc


class HashService(hash_service_pb2_grpc.HashServiceServicer):

    def _hash(self, msg):
        m = hashlib.sha256()
        m.update(msg.encode('utf-8'))
        return m.hexdigest()

    def GetEncoded(self, request, context):
        return hash_service_pb2.OutputMsg(value=self._hash(request.value))


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=2))
    hash_service_pb2_grpc.add_HashServiceServicer_to_server(HashService(),
                                                            server)
    server.add_insecure_port('[::]:22222')
    print('starting server...')
    server.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print('stopping server...')
        server.stop(0)


if __name__ == '__main__':
    serve()
