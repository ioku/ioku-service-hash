import grpc
import sys
import hash_service_pb2
import hash_service_pb2_grpc


def run():
    channel = grpc.insecure_channel('localhost:22222')
    stub = hash_service_pb2_grpc.HashServiceStub(channel)
    print(stub.GetEncoded(hash_service_pb2.InputMsg(value=sys.argv[1])).value)


if __name__ == "__main__":
    run()
