# ioku-service-hash

Simple service to hash strings


# DEV

- pipenv install --python 3.6  # this was ran for the initial setup
- pipenv shell
- python -m grpc_tools.protoc -I. --python_out=app --grpc_python_out=app hash_service.proto
- python app/server.py
- python app/client.py
- gitlab-ci-multi-runner exec docker --env CI_PIPELINE_ID="wherever" test
