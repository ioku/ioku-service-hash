import pytest
import grpc
import hash_service_pb2


@pytest.fixture(scope='module')
def grpc_add_to_server():
    from hash_service_pb2_grpc import add_HashServiceServicer_to_server
    return add_HashServiceServicer_to_server


@pytest.fixture(scope='module')
def grpc_servicer():
    from app.server import HashService
    return HashService()


@pytest.fixture(scope='module')
def grpc_channel():
    return grpc.insecure_channel('localhost:22222')


@pytest.fixture(scope='module')
def grpc_stub_cls(grpc_channel):
    from hash_service_pb2_grpc import HashServiceStub
    return HashServiceStub(grpc_channel)


def test_getencoded(grpc_stub_cls):
    request = hash_service_pb2.InputMsg(value='some string')
    response = grpc_stub_cls.GetEncoded(request)
    assert response.value == '61d034473102d7dac305902770471fd50f4c5b26f6831a56dd90b5184b3c30fc'
